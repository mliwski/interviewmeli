# MELI interview repo

This repository is intended to share the code for MELI technical interview. 

# Live DEMO

For API test intentions I published in heroku (super easy to deploy and test locally)
It could be visited at http://interviewmeli.herokuapp.com/swagger/

# How to install locally
To "install" the application, just have to run the commands:

```
npm install
```
and then 
```
grunt dev
```

How it works:

 - npm install: installs every package need to run the application.
 - grunt development: is a task used to work in a tdd style, it will serve the app at port 8000 and rebuild it in every file change.

Notes:

 - If you install heroku toolbet could also run the app with 
```
heroku local
```

- In some OS maybe you have to run 
```
npm install grunt -g
```
and 
```
npm install grunt-cli -g
```

# API docs

I used swagger to document the API. In this special case,if the application is in development mode (like with grunt dev) swagger UI will be served at  http://localhost:8000/swagger. In any other case i think that server swagger UI with the application could be a security pitfall.

Like you can see in the live DEMO I set the NODE_ENV in heroku in development too.


# Some assumptions and decisions

## General
 - I programmed in english to do not need to make a context switch between problem space and language syntax (even when i'm a little bit rusty)  
 - To solve the problem, I started I plan the API response in paper, then wrote the solutions architecture (also in paper), and then started to write in a TDD like way.
 - The processors (climate and shapes) are intended to extend to more planets or specific conditions
 - I created the 'uncertain climate' because it was not defined in the problem but exists when you have a triangle shape without the origin contained in it
 - To ensure API version I used header api (if is not present you could not use the API)
 - To check the coverage, run grunt compile and serve coverage/index.html 

## Live DEMO
 - Maybe the first time the application seems to be slow because heroku dyno its waking up.
 - The first time that calls report will be slow (3 sec, check response-time header) and then will be cached and response in 5 ms max.

## Why used node
 - I used NodeJS for this application, BUT it is not ideal for the main core problem because:
    - JS has a problem with floating point (LINK) to ignore it I truncated to 4 digits. 
    - NodeJS is single thread so to make the math is not ideal to use it because the calculations are CPU intensive and there is no threads to spread them.
 - Even with this pitfalls I choose NodeJS because:
    - I prefer to prioritize REST APIs easy build, reduce boilerplate, and start quickly. 
    - Do not need to serialize.
    - I intend to give a streaming mode and its really easy with node

# Wish list
 - More time to make another iteration to clean code
 - Full coverage
 - Add test stress
 - Make the application configurable live
 - Stream/pagination mode
 - Cache with loky/mongojs/memcached (mongojs or memcached are better to stream mode, an that is better to IO operations)
 - Remove TODO and FIXME
 - Be more elegant with cache initialization (in some services i used first call cache strategy, but in others I used system startup initialization)