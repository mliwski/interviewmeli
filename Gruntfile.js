'use strict';

module.exports = function(grunt) {

    var pkg = grunt.file.readJSON('package.json');

    // Project configuration.
    grunt.initConfig({
        env : {
            tests : {
                UNIT_TEST : grunt.option('unit-test'),
                COLLECTIONS_TEST : grunt.option('collections-test'),
                INTEGRATION_TEST : grunt.option('integration-test')
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                ignores: ['node_modules', 'src/libs/**/*.js', 'src/swagger/**']
            },
            config: ['Gruntfile.js'],
            src: ['src/**/*.js'],
            test: ['test/**/*.js']
        },
        mochaTest: {
            unit: {
                options: {
                    reporter: 'spec',
                    clearRequireCache: true
                },
                src: ['test/**/*.js']
            }
        },
        mocha_istanbul: {
            coverage: {
                src: 'src', // a folder works nicely
                options: {
                    mask: '*.js',
                    excludes: ['app.js'],
                    reporter: 'progress',
                    dryRun: false,
                    reportFormats:['html']
                }
            }
        },
        watch: {
            dev: {
                files: [
                    'src/**/*.js',
                    'test/**/*.js',
                    'Gruntfile.js'
                ],
                tasks: ['compile', 'develop'],
                options: { nospawn: true }
            }
        },
        develop: {
            server: {
                file: 'src/app.js',
                env: {
                    PORT: 8000,
                    NODE_ENV: 'development'
                }
            }
        }
    });

    grunt.registerTask('project_banner_task', 'Print project banner', function() {
        grunt.log.writeln("");
        grunt.log.writeln(" ################################");
        grunt.log.writeln(" #	" + pkg.name + " V" + pkg.version + "	#");
        grunt.log.writeln(" ################################");
    });

    grunt.registerTask('help_task', 'print help options', function() {
        grunt.log.writeln(" Grunt commands:");
        grunt.log.writeln(" ---------------");
        grunt.log.writeln(" * help : Print this options.");
        grunt.log.writeln(" * compile : Run JSHint and coverage (unit test and integration test included) to ensure sanity.");
        grunt.log.writeln(" * test : Run code tests.");
        grunt.log.writeln(" * coverage : Run code coverage.");
        grunt.log.writeln(" * dev : Run in development mode, on each change compiles and run again automatically.");
        grunt.log.writeln("");
        grunt.log.writeln(" Options:");
        grunt.log.writeln(" --------");
        grunt.log.writeln(" * tests (compile, test, coverage & tdd options):");
        grunt.log.writeln("     * --integration-test: [(true) | false] if sets to false don't prepare database for integration tests (By default sets to true).");
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-mocha-test');
    grunt.loadNpmTasks('grunt-mocha-istanbul');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-develop');
    grunt.loadNpmTasks('grunt-env');

    // Default task.
    grunt.registerTask('default', ['help']);
    grunt.registerTask('help', ['project_banner_task', 'help_task']);

    grunt.registerTask('test', ['env:tests', 'mochaTest']);
    grunt.registerTask('coverage', ['env:tests', 'mocha_istanbul:coverage']);

    grunt.registerTask('compile', ['jshint', 'coverage']);

    grunt.registerTask('dev', ['project_banner_task', 'compile', 'develop', 'watch:dev']);
};