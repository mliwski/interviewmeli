'use strict';

var chai = require('chai');
var expect = chai.expect;

var Planet = require('../../src/planets/planet');

describe("Planet ::", function() {
    var defaultPlanet = {
        name : "Earth",
        sunDistance : 2,
        angularVelocity : 44
    };
    var defaultDays = 4;

    describe("Construction ::", function(){
        it("Should build planet whit name", function(done) {
            var planet = new Planet(defaultPlanet);
            expect(planet).to.have.property('name').to.be.equal(defaultPlanet.name);
            done();

        });

        it("Should build planet whit distance", function(done) {
            var planet = new Planet(defaultPlanet);
            expect(planet).to.have.property('sunDistance').to.be.equal(defaultPlanet.sunDistance);
            done();

        });

        it("Should build planet whit angularVelocity", function(done) {
            var planet = new Planet(defaultPlanet);
            expect(planet).to.have.property('angularVelocity').to.be.equal(defaultPlanet.angularVelocity);
            done();

        });
    });

    describe("Position ::", function() {
        it("Should get position", function (done) {
            var planet = new Planet({
                sunDistance : 2,
                angularVelocity : 44
            });
            var days = 4;
            var calculatedPosition = {
                'y' : -0.1395,
                'x' : -1.9951
            };

            expect(planet.getPositionOnDay(days)).to.be.deep.equal(calculatedPosition);
            done();
        });

        it("Should not get position without sunDistance", function (done) {
            var planet = new Planet(defaultPlanet);
            delete planet.sunDistance;

            expect(planet.getPositionOnDay.bind(planet, defaultDays)).to.throw(Error);
            done();
        });

        it("Should not get position without angularVelocity", function (done) {
            var planet = new Planet(defaultPlanet);
            delete planet.angularVelocity;

            expect(planet.getPositionOnDay.bind(planet, defaultDays)).to.throw(Error);
            done();
        });

        it("Should not get position without day", function (done) {
            var planet = new Planet(defaultPlanet);
            expect(planet.getPositionOnDay.bind(planet, undefined)).to.throw(Error);
            done();
        });
    });

});