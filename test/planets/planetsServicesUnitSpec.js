'use strict';

var chai = require('chai')
    .use(require("chai-as-promised"));

var expect = chai.expect;

var rewire = require("rewire"); //Used to mock modules

//var BadInputError = require('../../src/commons/errors').BadInputError;

var planetsServices = rewire('../../src/planets/planetsServices');

var defaultPlanetsConfig = [{name : 'Ferengi', angularVelocity : 1, sunDistance : 500}];
var populatePlanets = planetsServices.__get__('populatePlanets');

describe("Planets Services ::", function() {
    describe("Initialization ::", function() {


        it("Should initialize planets from planets config", function(done) {
            //Rewire works after require, so initialization is made from real config and mocking will not work
            var planets = planetsServices.__get__('planets');
            expect(planets).to.have.length.of.at.least(1);
            done();
        });

        it("Should populate planets from config given", function(done) {
            var planetsConfigs = [{name : 'Ferengi', angularVelocity : 1, sunDistance : 500}];
            expect(populatePlanets(planetsConfigs)).to.have.length(defaultPlanetsConfig.length);
            done();
        });
    });

    describe("Planets ::", function() {
        planetsServices.__set__('planets', populatePlanets(defaultPlanetsConfig));

        it("Should get planets", function() {
            return expect(planetsServices.getPlanets()).to.eventually.have.length(defaultPlanetsConfig.length);
        });
    });
});