'use strict';

var chai = require('chai');
var expect = chai.expect;

var ProcessorChainBuilder = require('../../src/climates/processorChainBuilder');

describe("Shape processor chain builder ::", function() {

    it("Should not accept anything that is not a function", function (done) {
        var processorChainBuilder = new ProcessorChainBuilder();
        expect(processorChainBuilder.with.bind(processorChainBuilder, "")).to.throw(Error);
        done();
    });

    it("Should not accept objects without process method", function (done) {
        var processorChainBuilder = new ProcessorChainBuilder();
        expect(processorChainBuilder.with.bind(processorChainBuilder, {})).to.throw(Error);
        done();
    });

    it("Should not build a chain without processors", function (done) {
        var processorChainBuilder = new ProcessorChainBuilder();
        expect(processorChainBuilder.build.bind(processorChainBuilder)).to.throw(Error);
        done();
    });

    it("Should build a chain of processors", function (done) {
        function FakeProcessor(){}
        FakeProcessor.prototype.process = function(){return 'A';};

        var chain = new ProcessorChainBuilder()
            .with(FakeProcessor)
            .build();

        expect(chain.process()).to.be.equal('A');
        done();
    });

});