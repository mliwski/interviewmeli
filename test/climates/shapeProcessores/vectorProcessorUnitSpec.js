'use strict';

var chai = require('chai');
var expect = chai.expect;

var VectorProcessor = require('../../../src/climates/shapeProcessors/vectorProcessor');

describe("Vector shape processor ::", function() {

    describe("Shape evaluation behavior ::", function() {
        describe("Shape evaluation ::", function() {
            it("Should not return a vector shape if at least one point is not inline", function (done) {
                var positions = [{'x': -5, 'y': -2}, {'x': -2, 'y': 1}, {'x': 2, 'y': 3}];
                var fakeProcessor = {process: function() { return {shape:'not vector'}; }};
                var processor = new VectorProcessor(fakeProcessor);

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('not vector');
                done();
            });

            it("Should call next processor if dont have at least three positions to evaluate", function (done) {
                var fakeProcessor = {process: function() { return {shape:'not vector'}; }};
                var processor = new VectorProcessor(fakeProcessor);

                var shapeAnalysis = processor.process([]);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('not vector');
                done();
            });

            it("Should return a vector shape if every positions form a line with 45 degree", function (done) {
                var positions = [{'x': -5, 'y': -2}, {'x': -2, 'y': 1}, {'x': 2, 'y': 5}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('vector');
                done();
            });

            it("Should return a vector shape if every positions form a line parallel to y axis", function (done) {
                var positions = [{'x': 3, 'y': -2}, {'x': 3, 'y': 1}, {'x': 3, 'y': 5}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('vector');
                done();
            });

            it("Should return a vector shape if every positions form a line parallel to x axis", function (done) {
                var positions = [{'x': -5, 'y': 3}, {'x': -2, 'y': 3}, {'x': 2, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('vector');
                done();
            });
        });

        describe("Contains origin evaluation ::", function() {
            it("Should return contains origin flag in true on a line with 45 degree", function (done) {
                var positions = [{'x': 1, 'y': 1}, {'x': 2, 'y': 2}, {'x': 3, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(true);
                done();
            });

            it("Should return contains origin flag in true on a line parallel to y axis", function (done) {
                var positions = [{'x': 0, 'y': 1}, {'x': 0, 'y': 2}, {'x': 0, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(true);
                done();
            });

            it("Should return contains origin flag in true on a line parallel to x axis", function (done) {
                var positions = [{'x': 1, 'y': 0}, {'x': 2, 'y': 0}, {'x': 3, 'y': 0}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(true);
                done();
            });

            it("Should return contains origin flag in false on a line with 45 degree", function (done) {
                var positions = [{'x': 0, 'y': 1}, {'x': 1, 'y': 2}, {'x': 2, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(false);
                done();
            });

            it("Should return contains origin flag in false on a line parallel to y axis", function (done) {
                var positions = [{'x': 3, 'y': 1}, {'x': 3, 'y': 2}, {'x': 3, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(false);
                done();
            });

            it("Should return contains origin flag in false on a line parallel to x axis", function (done) {
                var positions = [{'x': 1, 'y': 3}, {'x': 2, 'y': 3}, {'x': 3, 'y': 3}];
                var processor = new VectorProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(false);
                done();
            });
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if positions are not in line", function (done) {
            var positions = [{'x': 0, 'y': 8}, {'x': 0, 'y': -8}, {'x': 8, 'y': 0}];
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new VectorProcessor(fakeProcessor);

            var shapeAnalysis = processor.process(positions);
            expect(shapeAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new VectorProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, [])).to.throw(Error);
            done();
        });
    });

});