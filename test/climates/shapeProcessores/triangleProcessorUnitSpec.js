'use strict';

var chai = require('chai');
var expect = chai.expect;

var TriangleProcessor = require('../../../src/climates/shapeProcessors/triangleProcessor');

describe("Triangle shape processor ::", function() {

    describe("Shape evaluation behavior ::", function() {
        describe("Shape evaluation ::", function() {
            it("Should not return a triangle if non triangle shape", function (done) {
                var positions = [{'x': -5, 'y': -2}, {'x': -2, 'y': -2}, {'x': 2, 'y': -2}];
                var fakeProcessor = {process: function() { return {shape:'not triangle'}; }};
                var processor = new TriangleProcessor(fakeProcessor);

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('not triangle');
                done();
            });

            it("Should call next processor if do not have at least three positions to evaluate", function (done) {
                var fakeProcessor = {process: function() { return {shape:'not triangle'}; }};
                var processor = new TriangleProcessor(fakeProcessor);

                var shapeAnalysis = processor.process([]);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('not triangle');
                done();
            });

            it("Should return a triangle shape without any point at any axis", function (done) {
                var positions = [{'x': 2, 'y': 5}, {'x': -3, 'y': 3}, {'x': 2, 'y': -2}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('triangle');
                done();
            });

            it("Should return a triangle shape with every point on an axis", function (done) {
                var positions = [{'x': 2, 'y': 0}, {'x': 0, 'y': 2}, {'x': -2, 'y': 0}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('triangle');
                done();
            });

            it("Should return a triangle shape if with every point on the same quadrant", function (done) {
                var positions = [{'x': -4, 'y': -2}, {'x': -6, 'y': -4}, {'x': -2, 'y': -4}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('shape').to.be.equal('triangle');
                done();
            });
        });

        describe("Contains origin evaluation ::", function() {
            it("Should return false on contains origin if the triangle is contained in the same quadrant", function (done) {
                var positions = [{'x': -4, 'y': -2}, {'x': -6, 'y': -4}, {'x': -2, 'y': -4}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(false);
                done();
            });

            it("Should return false on contains origin if the triangle is contained in distinct quadrants but do not contains origin", function (done) {
                var positions = [{'x': -4, 'y': 1}, {'x': -4, 'y': -5}, {'x': 2, 'y': -5}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(false);
                done();
            });

            it("Should return true on contains origin if the triangle pass through origin", function (done) {
                var positions = [{'x': 2, 'y': 0}, {'x': 0, 'y': 2}, {'x': -2, 'y': 0}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(true);
                done();
            });

            it("Should return true on contains origin if the triangle contains origin", function (done) {
                var positions = [{'x': 2, 'y': 0}, {'x': 0, 'y': 2}, {'x': -2, 'y': -1}];
                var processor = new TriangleProcessor({});

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('containsOrigin').to.be.equal(true);
                done();
            });
        });

        describe("Area evaluation ::", function() {
            it("Should return triangle area", function (done) {
                var positions = [{'x': 2, 'y': 0}, {'x': 3, 'y': 4}, {'x': -2, 'y': 5}];
                var processor = new TriangleProcessor({});
                var calculatedArea = parseFloat((21/2).toFixed(4));

                var shapeAnalysis = processor.process(positions);
                expect(shapeAnalysis).to.have.property('area').to.be.equal(calculatedArea);
                done();
            });
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if positions are in line", function (done) {
            var positions = [{'x': 1, 'y': 1}, {'x': 2, 'y': 2}, {'x': 3, 'y': 3}];
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new TriangleProcessor(fakeProcessor);

            var shapeAnalysis = processor.process(positions);
            expect(shapeAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new TriangleProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, [])).to.throw(Error);
            done();
        });
    });

});