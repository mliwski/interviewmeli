'use strict';

var chai = require('chai');
var expect = chai.expect;

var UncertainProcessor = require('../../../src/climates/climateProcessors/uncertainProcessor');

describe("Uncertain climate processor ::", function() {

    describe("Climate evaluation behavior ::", function() {
        it("Should not return uncertain climates if non triangle shape that do not contains origin", function (done) {
            var shapeAnalysis = {'shape': 'anyone', 'containsOrigin':false};
            var fakeProcessor = {process: function() { return {climate:'not uncertain'}; }};
            var processor = new UncertainProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not uncertain');
            done();
        });

        it("Should not return uncertain climates if triangle shape but contains origin", function (done) {
            var shapeAnalysis = {'shape': 'triangle', 'containsOrigin':true};
            var fakeProcessor = {process: function() { return {climate:'not uncertain'}; }};
            var processor = new UncertainProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not uncertain');
            done();
        });

        it("Should return uncertain climates if triangle shape and do not contains origin", function (done) {
            var shapeAnalysis = {'shape': 'triangle', 'containsOrigin':false};
            var processor = new UncertainProcessor();

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('uncertain');
            done();
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if this processor is not the right one", function (done) {
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new UncertainProcessor(fakeProcessor);

            var climateAnalysis = processor.process({shape:'unknown'});
            expect(climateAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new UncertainProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, {shape:'unknown'})).to.throw(Error);
            done();
        });
    });

});