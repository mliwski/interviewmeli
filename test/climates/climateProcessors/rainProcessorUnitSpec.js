'use strict';

var chai = require('chai');
var expect = chai.expect;

var RainProcessor = require('../../../src/climates/climateProcessors/rainProcessor');

describe("Rain climate processor ::", function() {

    describe("Climate evaluation behavior ::", function() {
        it("Should not return rain climates if non triangle shape that contains origin", function (done) {
            var shapeAnalysis = {'shape': 'anyone', 'containsOrigin':true};
            var fakeProcessor = {process: function() { return {climate:'not rain'}; }};
            var processor = new RainProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not rain');
            done();
        });

        it("Should not return rain climates if triangle shape but do not contains origin", function (done) {
            var shapeAnalysis = {'shape': 'triangle', 'containsOrigin':false};
            var fakeProcessor = {process: function() { return {climate:'not rain'}; }};
            var processor = new RainProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not rain');
            done();
        });

        it("Should return rain climates if triangle shape and contains origin", function (done) {
            var shapeAnalysis = {'shape': 'triangle', 'containsOrigin':true};
            var processor = new RainProcessor();

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('rain');
            done();
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if this processor is not the right one", function (done) {
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new RainProcessor(fakeProcessor);

            var climateAnalysis = processor.process({shape:'unknown'});
            expect(climateAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new RainProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, {shape:'unknown'})).to.throw(Error);
            done();
        });
    });

});