'use strict';

var chai = require('chai');
var expect = chai.expect;

var OptimumPressureAndTemperatureProcessor = require('../../../src/climates/climateProcessors/optimumPressureAndTemperatureProcessor');

describe("Optimum pressure and temperature climate processor ::", function() {

    describe("Climate evaluation behavior ::", function() {
        it("Should not return optimum pressure and temperature climates if non vector shape", function (done) {
            var shapeAnalysis = {'shape': 'anyone', 'containsOrigin':true};
            var fakeProcessor = {process: function() { return {climate:'not optimum pressure and temperature'}; }};
            var processor = new OptimumPressureAndTemperatureProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not optimum pressure and temperature');
            done();
        });

        it("Should not return optimum pressure and temperature climates if vector shape but contains origin", function (done) {
            var shapeAnalysis = {'shape': 'vector', 'containsOrigin':true};
            var fakeProcessor = {process: function() { return {climate:'not optimum pressure and temperature'}; }};
            var processor = new OptimumPressureAndTemperatureProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not optimum pressure and temperature');
            done();
        });

        it("Should return optimum pressure and temperature climates vector shape and do not contains origin", function (done) {
            var shapeAnalysis = {'shape': 'vector', 'containsOrigin':false};
            var processor = new OptimumPressureAndTemperatureProcessor();

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('optimum pressure and temperature');
            done();
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if this processor is not the right one", function (done) {
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new OptimumPressureAndTemperatureProcessor(fakeProcessor);

            var climateAnalysis = processor.process({shape:'unknown'});
            expect(climateAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new OptimumPressureAndTemperatureProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, {shape:'unknown'})).to.throw(Error);
            done();
        });
    });

});