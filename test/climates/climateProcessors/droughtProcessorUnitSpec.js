'use strict';

var chai = require('chai');
var expect = chai.expect;

var DroughtProcessor = require('../../../src/climates/climateProcessors/droughtProcessor');

describe("Drought climate processor ::", function() {

    describe("Climate evaluation behavior ::", function() {
        it("Should not return drought climates if non vector shape that contains origin", function (done) {
            var shapeAnalysis = {'shape': 'anyone', 'containsOrigin':true};
            var fakeProcessor = {process: function() { return {climate:'not drought'}; }};
            var processor = new DroughtProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not drought');
            done();
        });

        it("Should not return drought climates if vector shape but do not contains origin", function (done) {
            var shapeAnalysis = {'shape': 'vector', 'containsOrigin':false};
            var fakeProcessor = {process: function() { return {climate:'not drought'}; }};
            var processor = new DroughtProcessor(fakeProcessor);

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('not drought');
            done();
        });

        it("Should return drought if climates vector shape and contains origin", function (done) {
            var shapeAnalysis = {'shape': 'vector', 'containsOrigin':true};
            var processor = new DroughtProcessor();

            var climateAnalysis = processor.process(shapeAnalysis);
            expect(climateAnalysis).to.have.property('climate').to.be.equal('drought');
            done();
        });
    });

    describe("Chain of processors behavior ::", function() {
        it("Should call next processor if this processor is not the right one", function (done) {
            var fakeProcessor = {
                process: function() {
                    return "Im fake";
                }
            };
            var processor = new DroughtProcessor(fakeProcessor);

            var climateAnalysis = processor.process({shape:'unknown'});
            expect(climateAnalysis).to.be.equal('Im fake');
            done();
        });

        it("Should throw Error if not in line and non next processor given", function (done) {
            var processor = new DroughtProcessor();
            var processFunction = processor.process;
            expect(processFunction.bind(processor, {shape:'unknown'})).to.throw(Error);
            done();
        });
    });

});