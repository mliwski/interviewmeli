'use strict';
var config = require('./config');

var http = require('http');
var express = require('express');

//var httpLogger = require('morgan'); //HTTP requests logger see https://github.com/expressjs/morgan
var bodyParser = require('body-parser'); //Requests body-parser see https://github.com/expressjs/body-parser
var responseTime = require('response-time'); //Add response time info as header

var swaggerJSDoc = require('swagger-jsdoc');

var errors = require('./commons/errors');
var httpResponseCodes = require('./commons/httpResponseCodes');


/*****************************
 *        Express server     *
 *****************************/
var app = express();
app.set('x-powered-by', false);

app.use(responseTime());

//TODO Add logger - winston
if(app.get('env') === 'development') {
    console.log('Development mode');
}


/****************************
 *          Swagger         *
 ***************************/
if(app.get('env') === 'development') {

    //Swagger jsonDoc generated from jdDocs
    var options = {
        swaggerDefinition: {
            info: {
                title: 'Planets climates API',
                version: config.app.apiVersion
            },
            basePath: '/' ,
            produces: [
                "application/json"
            ],
            definitions: require('../swagger/definitions.json')
        },
        apis: [
            __dirname + '/climates/climatesRoutes.js',
            __dirname + '/planets/planetsRoutes.js',
            __dirname + '/reports/reportsRoutes.js'
        ] // Path to the API docs
    };

    // Initialize swagger-jsdoc -> returns validated swagger spec in json format
    var swaggerSpec = swaggerJSDoc(options);
    app.get('/swagger/api-docs.json', function(req, res) {
        res.setHeader('Content-Type', 'application/json');
        res.send(swaggerSpec);
    });

    //Swagger-ui serving
    app.use('/swagger', express.static(__dirname + '/../swagger'));
}


/*****************************
 *          API routes       *
 *****************************/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Handle API version
app.use(function(req, res, next) {
    if(config.app.apiVersion === req.get('api')) {
        next();
    } else {
        throw new errors.BadInputError('API version is incorrect');
    }
});


app.use('/planets', require('./planets/planetsRoutes'));
app.use('/climates', require('./climates/climatesRoutes'));
app.use('/reports', require('./reports/reportsRoutes'));


/****************************
 *      Error treatment     *
 ****************************/
// Catch 404
app.use(function(req, res, next) {
    res.status(404);
    next();
});

/* jshint unused: false */
app.use(function(error, req, res, next) {
    //TODO Change to error known vs unknown treatment
    var errorCode = httpResponseCodes.errorMapper(error);

    // production error handler no stacktraces leaked to user
    if(app.get('env') === 'development' && (error.stack || error.message)) {
        console.log("An error was catched at error handler:" + error.name + " : " + error.message + "-->" + error.stack);
    }

    res.header('Error', error.message || 'Unhandled error');
    res.status(errorCode);
    res.end();
});


/*************************************
 * NodeJS server (using express app) *
 *************************************/
var server = http.createServer(app);
server.listen(config.app.port);
server.on('error', onError);
server.on('listening', onListening);

// Event listener for HTTP server "error" event.
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof config.app.port === 'string' ? 'Pipe ' + config.app.port : 'Port ' + config.app.port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

// Event listener for HTTP server "listening" event.
function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
    console.log('Listening on ' + bind);
}