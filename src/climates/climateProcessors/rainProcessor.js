'use strict';

/**
 * Builds a processor to evaluate if the planets create a rain climate
 * @param next The next Processor
 * @constructor
 */
function RainProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

RainProcessor.prototype.process = function (shapeAnalysis) {
    var climateAnalysis = getClimateAnalysis(shapeAnalysis);

    if (climateAnalysis.climate === 'rain') {
        return climateAnalysis;
    } else if (this.next) {
        return this.next.process(shapeAnalysis);
    } else {
        throw new Error('Non climate processor was found, that satisfy the planets position');
    }
};

function getClimateAnalysis(shapeAnalysis) {
    var climateAnalysis = { climate : 'unknown'};
    if(shapeAnalysis.shape === 'triangle' && shapeAnalysis.containsOrigin === true) {
        climateAnalysis.climate = 'rain';
        climateAnalysis.humidity = shapeAnalysis.area;
    }

    return climateAnalysis;
}

module.exports = RainProcessor;