'use strict';

/**
 * Builds a processor to evaluate if the planets create an optimum pressure and temperature climate
 * @param next The next Processor
 * @constructor
 */
function OptimumPressureAndTemperatureProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

OptimumPressureAndTemperatureProcessor.prototype.process = function (shapeAnalysis) {
    var climateAnalysis = getClimateAnalysis(shapeAnalysis);

    if (climateAnalysis.climate === 'optimum pressure and temperature') {
        return climateAnalysis;
    } else if (this.next) {
        return this.next.process(shapeAnalysis);
    } else {
        throw new Error('Non climate processor was found, that satisfy the planets position');
    }
};

function getClimateAnalysis(shapeAnalysis) {
    var climateAnalysis = { climate : 'unknown'};
    if(shapeAnalysis.shape === 'vector' && shapeAnalysis.containsOrigin === false) {
        climateAnalysis.climate = 'optimum pressure and temperature';
    }

    return climateAnalysis;
}

module.exports = OptimumPressureAndTemperatureProcessor;