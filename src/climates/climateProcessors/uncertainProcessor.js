'use strict';

/**
 * Builds a processor to evaluate if the planets create an uncertain climate
 * @param next The next Processor
 * @constructor
 */
function UncertainProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

UncertainProcessor.prototype.process = function (shapeAnalysis) {
    var climateAnalysis = getClimateAnalysis(shapeAnalysis);

    if (climateAnalysis.climate === 'uncertain') {
        return climateAnalysis;
    } else if (this.next) {
        return this.next.process(shapeAnalysis);
    } else {
        throw new Error('Non climate processor was found, that satisfy the planets position');
    }
};

function getClimateAnalysis(shapeAnalysis) {
    var climateAnalysis = { climate : 'unknown'};
    if(shapeAnalysis.shape === 'triangle' && shapeAnalysis.containsOrigin === false) {
        climateAnalysis.climate = 'uncertain';
    }

    return climateAnalysis;
}

module.exports = UncertainProcessor;