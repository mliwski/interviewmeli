'use strict';

/**
 * Builds a processor to evaluate if the planets create a drought climate
 * @param next The next Processor
 * @constructor
 */
function DroughtProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

DroughtProcessor.prototype.process = function (shapeAnalysis) {
    var climateAnalysis = getClimateAnalysis(shapeAnalysis);

    if (climateAnalysis.climate === 'drought') {
        return climateAnalysis;
    } else if (this.next) {
        return this.next.process(shapeAnalysis);
    } else {
        throw new Error('Non climate processor was found, that satisfy the planets position');
    }
};

function getClimateAnalysis(shapeAnalysis) {
    var climateAnalysis = { climate : 'unknown'};
    if(shapeAnalysis.shape === 'vector' && shapeAnalysis.containsOrigin === true) {
        climateAnalysis.climate = 'drought';
    }

    return climateAnalysis;
}

module.exports = DroughtProcessor;