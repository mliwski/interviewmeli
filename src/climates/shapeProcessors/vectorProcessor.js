'use strict';

var _ = require('lodash');

//TODO Make a template method for common code (or something like that in JS)
//TODO Ensure right comments
//FIXME Add shape 'constants' and refer them instead string to normalize it
/**
 * Builds a processor to evaluate if the positions forms a vector shape
 * @param next The next Processor
 * @constructor
 */
function VectorProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

/**
 *
 * @param positions
 * @returns {{climate: string, sunInTheMiddle: boolean}}
 */
VectorProcessor.prototype.process = function (positions) {
    var shapeAnalysis = getShapeAnalysis(positions);

    if (shapeAnalysis.shape === 'vector') {
        return shapeAnalysis;
    } else if (this.next) {
        return this.next.process(positions);
    } else {
        throw new Error('Non shape processor was found, that satisfy the positions');
    }
};

//TODO Code better, this solution covers only three points case and also is not so clear
function getShapeAnalysis(positions) {
    var shapeAnalysis = {shape: 'unknown', containsOrigin: false};
    if(positions.length < 3) {
        return shapeAnalysis;
    }

    var pointOne = positions[0];
    var pointTwo = positions[1];
    var pointThree = positions[2];
    var origin = {x: 0, y: 0};

    if (pointThree.y === y(pointThree) || _.isNaN(y(pointThree))) {
        shapeAnalysis.shape = 'vector';
    }

    if (origin.y === y(origin) || (_.isNaN(y(pointThree)) && pointOne.x === 0)) {
        shapeAnalysis.containsOrigin = true;
    }
    return shapeAnalysis;

    function y(point) {
        return (point.x - pointOne.x) * Math.round((pointTwo.y - pointOne.y) / (pointTwo.x - pointOne.x)) + pointOne.y;
    }
}

module.exports = VectorProcessor;