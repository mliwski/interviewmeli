'use strict';

var _ = require('lodash');

/**
 * Builds a processor to evaluate if the positions forms a triangle shape
 * @param next The next Processor
 * @constructor
 */
function TriangleProcessor(next) {
    if (next) {
        /* jshint validthis: true */
        this.next = next;
    }
}

/**
 *
 * @param positions
 * @returns {{climate: string, sunInTheMiddle: boolean}}
 */
TriangleProcessor.prototype.process = function (positions) {
    var shapeAnalysis = getShapeAnalysis(positions);

    if (shapeAnalysis.shape === 'triangle') {
        return shapeAnalysis;
    } else if (this.next) {
        return this.next.process(positions);
    } else {
        throw new Error('Non shape processor was found, that satisfy the positions');
    }
};

//TODO Code better, this solution covers only three points case and also is not so clear
//FIXME Rename positions to points and convert to object dynamically
function getShapeAnalysis(positions) {
    var shapeAnalysis = {shape: 'unknown', containsOrigin: false};
    if(positions.length < 3) {
        return shapeAnalysis;
    }

    var pointOne = positions[0];
    var pointTwo = positions[1];
    var pointThree = positions[2];

    var isVector = (pointThree.y === y(pointThree) || _.isNaN(y(pointThree)));
    if (isVector) {
        return shapeAnalysis;
    } else {
        shapeAnalysis.shape = 'triangle';
    }

    shapeAnalysis.containsOrigin = isOriginContained(positions);


    //TODO Get the area with Vector Modulus and Heron Formula
    shapeAnalysis.area = getTriangleArea();


    return shapeAnalysis;

    function y(point) {
        return (point.x - pointOne.x) * Math.round((pointTwo.y - pointOne.y) / (pointTwo.x - pointOne.x)) + pointOne.y;
    }

    function isOriginContained() {
        var origin = {x: 0, y: 0};

        var triangleZ = (pointOne.x - pointThree.x) * (pointTwo.y - pointThree.y) * (pointOne.y - pointThree.y) * (pointTwo.x - pointThree.x);
        var triangleOneZ = (pointOne.x - pointThree.x) * (origin.y - pointThree.y) * (pointOne.y - pointThree.y) * (origin.x - pointThree.x);
        var triangleTwoZ = (pointOne.x - origin.x) * (pointTwo.y - origin.y) * (pointOne.y - origin.y) * (pointTwo.x - origin.x);
        var triangleThreeZ = (origin.x - pointThree.x) * (pointTwo.y - pointThree.y) * (origin.y - pointThree.y) * (pointTwo.x - pointThree.x);

        var triangleOrientation = triangleZ >= 0 ? 1 : -1;
        var triangleAOrientation = triangleOneZ >= 0 ? 1 : -1;
        var triangleBOrientation = triangleTwoZ >= 0 ? 1 : -1;
        var triangleCOrientation = triangleThreeZ >= 0 ? 1 : -1;

        return (triangleOrientation === triangleAOrientation &&  triangleAOrientation === triangleBOrientation && triangleBOrientation === triangleCOrientation);
    }

    function getTriangleArea() {
        var vectorOneModulus = Math.sqrt(Math.pow(pointTwo.x - pointOne.x,2) + Math.pow(pointTwo.y - pointOne.y,2));
        var vectorTwoModulus = Math.sqrt(Math.pow(pointThree.x - pointTwo.x,2) + Math.pow(pointThree.y - pointTwo.y,2));
        var vectorThreeModulus = Math.sqrt(Math.pow(pointOne.x - pointThree.x,2) + Math.pow(pointOne.y - pointThree.y,2));

        var semiPerimeter = (vectorOneModulus + vectorTwoModulus + vectorThreeModulus) / 2;

        var area = Math.sqrt(semiPerimeter * (semiPerimeter - vectorOneModulus) * (semiPerimeter - vectorTwoModulus) * (semiPerimeter - vectorThreeModulus));
        return parseFloat(area.toFixed(4));
    }
}

module.exports = TriangleProcessor;