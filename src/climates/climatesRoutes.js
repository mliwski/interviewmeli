'use strict';
var express = require('express');
var router = express.Router();

var responseCode = require('../commons/httpResponseCodes');

var climatesServices = require('./climatesServices');

/************************
 *    Climates Routes   *
 ************************/

/**
 * @swagger
 * /climates/:
 *    get:
 *      tags:
 *        - Climates
 *      summary: Returns solar system climate forecast
 *      description: Returns the solar system climates from day 0 to the sent days forecast. If page is given, then return will be paginated else iit will be streamed.
 *      parameters:
 *        - name: days
 *          in: query
 *          description: Days to forecast
 *          required: true
 *          type: integer
 *        - name: page
 *          in: query
 *          description: Page to return in pagination mode
 *          required: false
 *          type: integer
 *        - name: size
 *          in: query
 *          description: Size of the page to return in pagination mode (With a maximum of 100 elements)
 *          required: false
 *          type: integer
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Climates
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Climate'
 */
router.get('/', function(req, res, next) {
    climatesServices.getClimates(req.query).then(function(result) {
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});


/**
 * @swagger
 * /climates/{day}:
 *    get:
 *      tags:
 *        - Climates
 *      summary: Returns solar system climate for a single day
 *      description: Returns the solar system climates for the selected day.
 *      parameters:
 *        - name: day
 *          in: path
 *          description: Days to get the climate
 *          required: true
 *          type: integer
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Climate
 *          schema:
 *            $ref: '#/definitions/Climate'
 */
router.get('/:day/', function(req, res, next) {
    climatesServices.getClimateOnDay(req.params.day).then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

module.exports = router;