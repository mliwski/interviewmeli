'use strict';
var BluePromise = require('bluebird');
var app = require('express')();

var config = require('../config');

var BadInputError = require('../commons/errors').BadInputError;

var planetsServices = require('../planets/planetsServices');

var VectorProcessor = require('./shapeProcessors/vectorProcessor');
var TriangleProcessor = require('./shapeProcessors/triangleProcessor');

var RainProcessor = require('./climateProcessors/rainProcessor');
var UncertainProcessor = require('./climateProcessors/uncertainProcessor');
var OptimumPressureAndTemperatureProcessor = require('./climateProcessors/optimumPressureAndTemperatureProcessor');
var DroughtProcessor = require('./climateProcessors/droughtProcessor');

var ProcessorChainBuilder = require('./processorChainBuilder');
var shapeProcessor = new ProcessorChainBuilder()
    .with(TriangleProcessor)
    .with(VectorProcessor)
    .build();
var climateProcessor = new ProcessorChainBuilder()
    .with(RainProcessor)
    .with(UncertainProcessor)
    .with(OptimumPressureAndTemperatureProcessor)
    .with(DroughtProcessor)
    .build();

var climatesServices = {};

/**
 * Return the climate for the next days {{days}} quantity
 * @returns Array of climates
 */
climatesServices.getClimates = function(params) {
    var climates = [];

    //TODO: Add better params verification
    var daysToForecast = params && params.days ? params.days : config.report.forecast.days;
    for(var i = 0; i <= daysToForecast; i++) {
        climates.push(climatesServices.getClimateOnDay(i));
    }
    return BluePromise.all(climates);
};

/**
 * Returns the climate based on planets positions on day {{day}}
 * @param day The day to analyse the climate
 * @returns An object with day and climate (Promised)
 */
climatesServices.getClimateOnDay = function(day) {
    return checkStructuralPreconditions(day)
        .then(getPlanetsPositionsOnDay)
        .then(getShapeAnalysisByPositions)
        .then(getClimateAnalysisByShape)
        .then(buildResponse)
        .catch(logErrorAndReject);

    function checkStructuralPreconditions(day) {
        if(day === undefined) {
            return BluePromise.reject(new BadInputError('You have to give a day to make my predictions'));
        }
        return BluePromise.resolve(day);
    }

    function buildResponse(climateAnalysis) {
        var response = {};
        response.climate = climateAnalysis.climate;
        response.day = day;

        if(climateAnalysis.climate === 'rain') {
            response.humidity = climateAnalysis.humidity;
        }

        return BluePromise.resolve(response);
    }
};

function getPlanetsPositionsOnDay(day) {
    return planetsServices.getPositionsOnDay(day);
}

function getShapeAnalysisByPositions(planetPositions) {
    return BluePromise.resolve(shapeProcessor.process(planetPositions.positions));
}

function getClimateAnalysisByShape(shapeAnalysis) {
    return BluePromise.resolve(climateProcessor.process(shapeAnalysis));
}

function logErrorAndReject(error) {
    if (app.get('env') === 'development') {
        console.warn(error);
    }
    return BluePromise.reject(error);
}

module.exports = climatesServices;