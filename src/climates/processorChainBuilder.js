'use strict';

var _ = require('lodash');

/**
 * Builds a ShapeProcessor chain
 * @constructor
 */
function ShapeProcessorChainBuilder() {
    this.processorsArray = [];
}

ShapeProcessorChainBuilder.prototype.with = function(Processor) {
    var isNotAProcessor = _.isFunction(Processor) === false || _.functions(new Processor()).indexOf('process') === -1;
    if(isNotAProcessor) {
        throw new Error('Could not build a chain of processors with something else than processors');
    }

    this.processorsArray.push(Processor);
    return this;
};

ShapeProcessorChainBuilder.prototype.build = function () {
    if(this.processorsArray.length === 0) {
        throw new Error('Could not build a chain of processors without processors');
    }

    return this.processorsArray.reduce(function(CurrentProcessor, LastProcessor) {
        return new LastProcessor(CurrentProcessor);
    }, undefined);
};

module.exports = ShapeProcessorChainBuilder;