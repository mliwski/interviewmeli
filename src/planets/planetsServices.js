'use strict';
var BluePromise = require('bluebird');
var _ = require('lodash');
var app = require('express')();

var NotFoundError = require('../commons/errors').NotFoundError;
var BadInputError = require('../commons/errors').BadInputError;

var config = require('../config');

var Planet = require('./planet');
var planets = populatePlanets(config.planets);

//TODO Add planet name and orbit check
//TODO Publish this service so you can restart the system if config is changed
function populatePlanets(planetsConfig) {
    return planetsConfig.map(function(planetConfig) {
        return new Planet(planetConfig);
    });
}

var planetsServices = {};

/**
 * Return the planets found in this solar system
 * @returns Array of planets
 */
planetsServices.getPlanets = function() {
    return BluePromise.resolve(planets);
};

/**
 * Returns a planet whit name {{name}}
 * @param name The name of planet to be searched
 * @returns The Planet found (Promised)
 */
planetsServices.getPlanetByName = function(name) {
    return checkStructuralPreconditions(name)
        .then(normalizeInput)
        .then(findPlanetByName)
        .catch(logErrorAndReject);

    function checkStructuralPreconditions(name) {
        if(_.isUndefined(name) ) {
            return BluePromise.reject(new BadInputError('You have to provide me a name to find the planet.'));
        }

        return BluePromise.resolve(name);
    }

    function normalizeInput(name) {
        return BluePromise.resolve(name.toLowerCase());
    }
};

function findPlanetByName(name) {
    var filteredPlanets = planets.filter(function(planet) {
        return planet.name.toLowerCase() === name;
    });

    if(filteredPlanets.length === 0) {
        return BluePromise.reject(new NotFoundError('I can not find a planet with the given name.'));
    }

    return BluePromise.resolve(filteredPlanets[0]);
}

/**
 * Returns the positions of each planet for 10 years
 * @params If want pagination give an object with page and size, else if you want streamed response leave it undefined.
 * @returns An object with x,y position of the planet (Promised for pagination or Streamed if non params given)
 */
planetsServices.getPositions = function(params) {
    var positions = [];

    //TODO: Add better params verification
    var daysToForecast = params && params.days ? params.days : config.report.forecast.days;
    for(var i = 0; i <= daysToForecast; i++) {
        positions.push(planetsServices.getPositionsOnDay(i));
    }
    return BluePromise.all(positions);
};


/**
 * Returns the position of planet {{params.name}} in {{params.day}} day
 * @param params Object with name and day to found the position
 * @returns An object with x,y position of the planet (Promised)
 */
planetsServices.getPositionOnDay = function(params) {
    return checkStructuralPreconditions(params)
        .then(normalizeInput)
        .then(findPlanet)
        .then(getPlanetPosition)
        .then(buildResponse)
        .catch(logErrorAndReject);

    function checkStructuralPreconditions(params) {
        if(_.isUndefined(params.name) ) {
            return BluePromise.reject(new BadInputError('You have to provide me a name to find the planet.'));
        }
        if(_.isUndefined(params.day) ) {
            return BluePromise.reject(new BadInputError('You have to provide me a day to find the planet position.'));
        }

        return BluePromise.resolve(params);
    }

    function normalizeInput(params) {
        params.name = params.name.toLowerCase();
        params.day = parseInt(params.day);
        return BluePromise.resolve(params);
    }

    function findPlanet (params) {
        return findPlanetByName(params.name).then(buildResponse);

        function buildResponse(planet) {
            return {
                name: params.name,
                planet: planet,
                day: params.day
            };
        }
    }

    function getPlanetPosition(params) {
        var planet = params.planet;
        var day = params.day;
        return BluePromise.resolve(planet.getPositionOnDay(day));
    }

    function buildResponse(position) {
        var response = {
            name: params.name,
            x: position.x,
            y: position.y
        };
        return BluePromise.resolve(response);
    }
};

/**
 * Returns the position of each planet on day {{day}}
 * @param day The day to search positions
 * @returns Array of objects with x,y position of each planet (Promised)
 */
planetsServices.getPositionsOnDay = function(day) {
    return BluePromise.map(planets, getPositionForEachPlanet)
        .then(buildResponse);

    function getPositionForEachPlanet (planet) {
        return planetsServices.getPositionOnDay({name: planet.name, day: day});
    }

    function buildResponse(positions) {
        return {day: day, positions: positions};
    }
};

function logErrorAndReject(error) {
    if (app.get('env') === 'development') {
        console.warn(error);
    }
    return BluePromise.reject(error);
}

module.exports = planetsServices;