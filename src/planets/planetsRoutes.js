'use strict';
var express = require('express');
var router = express.Router();

var responseCode = require('../commons/httpResponseCodes');

var planetsServices = require('./planetsServices');
//IMPORTANT!!! Be careful with routes declaration, /planets/positions need to be before /planets
/**********************************
 *    Planets Positions Routes    *
 **********************************/

/**
 * @swagger
 * /planets/positions/:
 *    get:
 *      tags:
 *        - Planets
 *      summary: Returns solar system planets positions
 *      description: Returns the solar system planets positions from day 0 to the sent days forecast. If page is given, then return will be paginated else iit will be streamed.
 *      parameters:
 *        - name: page
 *          in: query
 *          description: Page to return in pagination mode
 *          required: false
 *          type: integer
 *        - name: size
 *          in: query
 *          description: Size of the page to return in pagination mode (With a maximum of 100 elements)
 *          required: false
 *          type: integer
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Planets Positions
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/PlanetPosition'
 */
router.get('/positions/', function(req, res, next) {
    //TODO Implement when define how to implement pagination and streams

    planetsServices.getPositions(req.query).then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

/**
 * @swagger
 * /planets/positions/{day}:
 *    get:
 *      tags:
 *        - Planets
 *      summary: Returns solar system planets position for a single day
 *      description: Returns the solar system planet positions for the selected day.
 *      parameters:
 *        - name: day
 *          in: path
 *          description: Day to get planets positions
 *          required: true
 *          type: integer
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Planets Positions
 *          schema:
 *            $ref: '#/definitions/PlanetPosition'
 */
router.get('/positions/:day', function(req, res, next) {
    planetsServices.getPositionsOnDay(req.params.day).then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

/**
 * @swagger
 * /planets/{name}/positions/{day}:
 *    get:
 *      tags:
 *        - Planets
 *      summary: Returns solar system planets position for a single planet in a single day
 *      description: Returns the solar system planet positions for the selected day.
 *      parameters:
 *        - name: name
 *          in: path
 *          description: The name of the planet
 *          required: true
 *          type: string
 *        - name: day
 *          in: path
 *          description: Day to get the planet position
 *          required: true
 *          type: integer
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Planet Position
 *          schema:
 *            $ref: '#/definitions/Position'
 */
router.get('/:name/positions/:day', function(req, res, next) {
    planetsServices.getPositionOnDay(req.params).then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

/************************
 *    Planets Routes    *
 ************************/

/**
 * @swagger
 * /planets/:
 *    get:
 *      tags:
 *        - Planets
 *      summary: Returns solar system planets description
 *      description: Returns the solar system planet description.
 *      parameters:
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Planet
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Planet'
 */
router.get('/', function(req, res, next) {
    planetsServices.getPlanets().then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

/**
 * @swagger
 * /planets/{name}:
 *    get:
 *      tags:
 *        - Planets
 *      summary: Returns single description
 *      description: Returns single planet description.
 *      parameters:
 *        - name: name
 *          in: path
 *          description: The name of the planet
 *          required: true
 *          type: string
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Planet
 *          schema:
 *            $ref: '#/definitions/Planet'
 */
router.get('/:name/', function(req, res, next) {
    planetsServices.getPlanetByName(req.params.name).then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});
module.exports = router;