'use strict';

var _ = require('lodash');

/**
 * Planet model.
 */
function Planet(params) {
    if(_.isObject(params) && _.isEmpty(params) === false) {
        this.name = params.name;
        this.sunDistance = params.sunDistance;
        this.angularVelocity = params.angularVelocity;
    }
}

Planet.prototype.getPositionOnDay = function(day) {
    if(this.sunDistance === undefined || this.angularVelocity === undefined) {
        throw new Error('You have to use this function in an object with sunDistance & angularVelocity');
    }
    if(day === undefined) {
        throw new Error('You have to give a day to make my math');
    }

    var y = Math.sin((day * this.angularVelocity * -1 * Math.PI) / 180) * this.sunDistance;
    var x = Math.cos((day * this.angularVelocity * -1 * Math.PI) / 180) * this.sunDistance;
    return {
        'x':parseFloat(x.toFixed(4)),
        'y':parseFloat(y.toFixed(4))
    };
};

module.exports = Planet;