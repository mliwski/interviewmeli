//TODO Evaluar si no conviene pasarlo a yaml que es mas legible y tiene menos boilerplate
'use strict';

var config = {};

config.app = {};
config.app.port = process.env.PORT || 3000;
config.app.apiVersion = '1.0'; //TODO Ver como obtener del proceso de node (sacandole el ultimo digito)

config.planets = [];
config.planets.push({name : 'Ferengi', angularVelocity : 1, sunDistance : 500});
config.planets.push({name : 'Betasoide', angularVelocity : 3, sunDistance : 2000});
config.planets.push({name : 'Vulcano', angularVelocity : -5, sunDistance : 1000});

config.report = {};
config.report.forecast = {};
config.report.forecast.days=3650;
config.report.forecast.maxPageSize=50;


module.exports = config;
