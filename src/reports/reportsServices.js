'use strict';
var BluePromise = require('bluebird');
var _ = require('lodash');

var config = require('../config');
var climatesServices = require('../climates/climatesServices');

var reportsServices = {};

var climatesCache = [];

/**
 * Return the climate for the next days {{days}} quantity
 * @returns Array of climates
 */
reportsServices.getReports = function() {
    return getClimates()
        .then(groupClimatesByClimateName)
        .then(addPeriodsReport)
        .then(addMaxRainDayReport)
        .then(buildReportResponse);

    function getClimates() {
        if (_.isEmpty(climatesCache)) {
            return climatesServices.getClimates({days: config.report.forecast.days}).then(function (climates) {
                climatesCache = climates;
                return climates;
            });
        } else {
            return BluePromise.resolve(climatesCache);
        }
    }

    function groupClimatesByClimateName(climates) {
        var climatesGrouped = _.groupBy(climates, function (climate) {
            return climate.climate;
        });
        return BluePromise.resolve(climatesGrouped);
    }

    function addPeriodsReport(climatesGrouped) {
        var report = {};
        report.climatesGrouped = climatesGrouped;
        report.periods = {
            'rain days': climatesGrouped.rain ? climatesGrouped.rain.length : 0,
            'drought days': climatesGrouped.drought ? climatesGrouped.drought.length : 0,
            'optimum pressure and temperature days': climatesGrouped['optimum pressure and temperature'] ? climatesGrouped['optimum pressure and temperature'].length : 0,
            'uncertain days': climatesGrouped.uncertain ? climatesGrouped.uncertain.length : 0
        };

        return BluePromise.resolve(report);
    }

    function addMaxRainDayReport(report) {
        var maxHumidityDay = _.max(report.climatesGrouped.rain, function (period) {
            return period.humidity;
        });
        report['maximum rain day number'] = maxHumidityDay.day;

        return BluePromise.resolve(report);
    }

    function buildReportResponse(report) {
        delete report.climatesGrouped;

        return BluePromise.resolve(report);
    }

};

module.exports = reportsServices;