'use strict';
var express = require('express');
var router = express.Router();

var responseCode = require('../commons/httpResponseCodes');

var reportsServices = require('./reportsServices');

/************************
 *     Reports Routes   *
 ************************/

/**
 * @swagger
 * /reports/:
 *    get:
 *      tags:
 *        - Reports
 *      summary: Returns solar system forecast
 *      description: Returns the solar system forecast for the next 10 years.
 *      parameters:
 *        - name: api
 *          in: header
 *          description: API version
 *          required: true
 *          type: string
 *          default: '1.0'
 *      responses:
 *        '200':
 *          description: Forecast
 *          schema:
 *            $ref: '#/definitions/Forecast'
 */
router.get('/', function(req, res, next) {
    reportsServices.getReports().then(function(result){
        res.status(responseCode.ok);
        res.json(result);
    }).catch(next);
});

module.exports = router;