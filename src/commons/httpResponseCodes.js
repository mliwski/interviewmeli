'use strict';

var errors = require('./errors');

module.exports = {
//Ok responses
    ok:'200',
    created : '201',
    deleted : '204',
//TODO Check that this errors are not used and delete them (for this errors is the mapper created)
//Recoverable error responses
    badformed : '400',
    unauthorized : '401',
    notFound : '404',
    notAllowed : '405',
    conflict : '409',

    errorMapper: function (error) {
        //TODO: Find a better implementation
        var status = 500;

        if (error instanceof errors.NotFoundError) {
            status = 404;
        } else if (error instanceof errors.BadInputError) {
            status = 400;
        } else if (error instanceof errors.UnauthorizedError) {
            status = 401;
        } else if (error instanceof errors.AlreadyExistsError) {
            status = 409;
        } else if (error instanceof errors.TimeoutError) {
            status = 408;
        } else if (error instanceof errors.UnimplementedError) {
            status = 405;
        }

        return status;
    }
};
