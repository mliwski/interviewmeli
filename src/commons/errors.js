'use strict';

//FIXME Split in several files for each error, add the class XError with translateToHTTPError method and subclass the error from this class not from Error

/**
 * Error defined to reflect that the searched object was not found.
 */
function NotFoundError(message) {
    this.name = 'NotFoundError';
    this.message = message || 'Object not found';
}
NotFoundError.prototype = Object.create(Error.prototype);
NotFoundError.prototype.constructor = NotFoundError;

/**
 * Error defined to reflect that some input are not well sent.
 */
function BadInputError(message) {
    this.name = 'BadInputError';
    this.message = message || 'An input field or fields are not well sent';
}
BadInputError.prototype = Object.create(Error.prototype);
BadInputError.prototype.constructor = BadInputError;

/**
 * Error defined to reflect that the object already exists.
 */
function AlreadyExistsError(message) {
    this.name = 'AlreadyExistsError';
    this.message = message || 'Object already exists';
}
AlreadyExistsError.prototype = Object.create(Error.prototype);
AlreadyExistsError.prototype.constructor = AlreadyExistsError;

/**
 * Error defined to reflect that the access to the resource is unauthorized.
 */
function UnauthorizedError(message) {
    this.name = 'UnauthorizedError';
    this.message = message || 'The access to the resource is unauthorized';
}
UnauthorizedError.prototype = Object.create(Error.prototype);
UnauthorizedError.prototype.constructor = UnauthorizedError;

/**
 * Error defined to reflect a timeout happened.
 */
function TimeoutError(message) {
    this.name = 'TimeoutError';
    this.message = message || 'Request to client timed out';
}
TimeoutError.prototype = Object.create(Error.prototype);
TimeoutError.prototype.constructor = TimeoutError;

/**
 * Error defined to reflect something unimplemented
 */
function UnimplementedError(message) {
    this.name = 'UnimplementedError';
    this.message = message || 'This is not implemented yet';
}
UnimplementedError.prototype = Object.create(Error.prototype);
UnimplementedError.prototype.constructor = UnimplementedError;


module.exports = {
    NotFoundError: NotFoundError,
    BadInputError: BadInputError,
    AlreadyExistsError: AlreadyExistsError,
    UnauthorizedError: UnauthorizedError,
    TimeoutError: TimeoutError,
    UnimplementedError: UnimplementedError
};